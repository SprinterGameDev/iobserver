var config = require('./Config.js');

module.exports = Base;


function Base(mysql) {
    this.EVENT_MYSQL_ERROR = 'error';
    this.TOP_SORT_BY_SCORE = 'points';
    this.TOP_FROM_LEVEL = 'level';
    
    this._mysql = mysql;
    
    this.HOST = config.MYSQL_HOST;
    this.PORT = config.MYSQL_PORT;
    this.MYSQL_USER = config.MYSQL_USER;
    this.MYSQL_PASS = config.MYSQL_PASS;
    this.DATABASE = config.MYSQL_DATABASE;
    this.mysql = null;
}

Base.prototype.init = function() {
    this.mysql = this._mysql.createConnection({
        host: this.HOST,
        port: this.PORT,
        user: this.MYSQL_USER,
        password: this.MYSQL_PASS
    });

    this.mysql.connect();
    this.mysql.query('use ' + this.DATABASE);
    this.handleDisconnect(this.mysql);
}

Base.prototype.handleDisconnect = function (client) {
    var self = this;
    this.mysql.on(this.EVENT_MYSQL_ERROR, function (error) {
        if (!error.fatal) return;
        if (error.code !== 'PROTOCOL_CONNECTION_LOST') throw Error (error);
        console.error('> Re-connecting lost MySQL connection: ' + error.stack);
        self.init();
    });
};
