/**
 * Created by Админ on 13.09.14.
 */
var config = require('../Config.js');
var crypto = require(config.MODULE_PATH + 'crypto');
var baseModel = require('./BaseModel.js');
var extend = require('../Extend.js');
extend(UserModel, baseModel);

module.exports = UserModel;

function UserModel(model){
    UserModel.__super__.constructor.apply(this, arguments);

    var _salt = "DS6DUY4BSU7CXVBGJ36FGVG";

    this.set(model);

    this.isAuthUser = function() {
        return Boolean(this.get('id'));
    };

    this.getUserData = function() {
        var data = {
            id: this.get('id'),
            name: this.get('name'),
            login: this.get('login'),
            observed: this.get('observed'),
            serviceToken: this.get('serviceToken')
        };
        return data;
    };

    this.generateServiceToken = function() {
        var hash = crypto.createHash('md5').update(
            this.get('id') + this.get('login') + _salt
        ).digest("hex");

        this.set({serviceToken: hash});
    };

    this.verifyServiceToken = function(token, id, login) {

        var hash = crypto.createHash('md5').update(
                id + login + _salt
        ).digest("hex");

        return token == hash;
    };

    this.remove = function() {
        this._params = [];
        console.log('UserModel remove');
        //UserModel.__super__.remove.apply(this, []);
    };
}
