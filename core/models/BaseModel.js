/**
 * Created by Админ on 13.09.14.
 */
module.exports = BaseModel;

function BaseModel() {
    var self = this;
    this._params = new Object();

    this.set = function (modelData) {
        if (typeof(modelData) === 'object') {
            for (var key in modelData) {
                self._params[key] = modelData[key];
            }
        }
    };

    this.get = function (key) {
        return self._params[key];
    };

    this.remove = function() {
        for (var key in this) {
            this[key] = null;
        }
        console.log('BaseModel remove');
    }

    this.sendMessage = function (event, options) {
        var params = {event: event, data: options};
        if (this.get('socket')) {
            this.get('socket').emit('event', params);
        }
    };
}
