var baseModel = require('./BaseModel.js');
var extend = require('../Extend.js');
extend(ServiceModel, baseModel);

module.exports = ServiceModel;

function ServiceModel(model){
    ServiceModel.__super__.constructor.apply(this, arguments);

    this.set(model);

    this.isAuthUser = function() {
        return false;
    };
}