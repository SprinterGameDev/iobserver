/**
 * Created by Админ on 18.09.14.
 */
var events = require('./Events.js');
module.exports = new SyncController();

function SyncController() {
    this._sericeCollection = null;
    this._userCollection = null;
}

SyncController.prototype.applyCollection = function(serviceCollection, userCollection) {
    this._sericeCollection = serviceCollection;
    this._userCollection = userCollection;
};

SyncController.prototype.sendMessage = function(senderSocketId, message) {
    if (!this._sericeCollection || !this._userCollection || !senderSocketId || !message) {
        return;
    }
    var serviceModel = this._sericeCollection.getModel(senderSocketId);
    if (!serviceModel) return;
    var objerved = serviceModel.get('observed');
    for (var i = 0; i < objerved.length; i++) {
        var observedInfo = objerved[i];
        var userModel = this._userCollection.getModelById(observedInfo.id);
        console.log('find observeds model', userModel);
        if (typeof(userModel) == 'object') {
            message.id = serviceModel.get('id');
            userModel.sendMessage(events.ON_SYNC_COORD, message);
        }
    }
};
