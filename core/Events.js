/**
 * Created by Админ on 13.09.14.
 */

module.exports = new Events();

function Events() {
    this.AUTH = 'auth';
    this.ON_AUTH = 'onAuth';
    this.USER_DISCONNECT = 'userDisconnect';

    this.CONFIG = 'config';
    this.ON_CONFIG = 'onConfig';

    this.SAVE_SETTINGS = 'saveSettings';
    this.ON_SAVE_SETTINGS = 'onSaveSettings';

    this.ADD_FRIEND = 'addFriend';
    this.ON_ADD_FRIEND = 'onAddFriend';

    this.SEND_COORD = 'sendCoord';
    this.ON_SYNC_COORD = 'onSyncCoord';

    this.ON_USER_DISCONNECT = 'onUserDisconnect';
    this.ON_USER_CONNECTED = 'onUserConnected';

    this.AUTH_SERVICE = "authService";
    this.ON_AUTH_SERVICE = "onAuthService";
}
