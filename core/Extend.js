module.exports = extend;

function extend(Child, Parent) {
    (Child.prototype = Object.create(Child.__super__ = Parent.prototype)).constructor = Child;
}
