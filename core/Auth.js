exports.base = null;
exports.mysql = {};

var ERRORS = {
    1: 'Пользователь не найден!',
    2: 'Данный номер уже зарегистрирован!',
    3: 'Вы не можете добавить свой номер!',
    4: 'Пароль или номер телефона не верен!'
};

var subParams = {};

exports.changePassword = function(id, newPassword, callback) {
    var data = {password: newPassword};

    var query = exports.base.mysql.query(
            'update auth set pass="' + newPassword + '" where id=' + id,
            function (err, result) {
                if (err) {
                    data.error = true;
                    callback(data);
                    console.log("Query error:" + err);
                } else {
                    data.error = false;
                    callback(data);
                }
            }
    );
};

exports.changeName = function(id, value, callback) {
    var data = {'name': value};
        var query = exports.base.mysql.query(
                'update users set name="' + value + '" where id=' + id,
                function (err, result) {
                    if (err) {
                        data.error = true;
                        callback(data);
                        console.log("Query error:" + err);
                    } else {
                        data.error = false;
                        callback(data);
                    }
                }
        );
};

function createUser(login, pass, name, callback) {
    var query = exports.base.mysql.query(
        'insert into auth (login, pass) values ("' + login + '", "' + pass + '")',
        function (err, result) {
            if (err) {
                callback(err);
                console.log("Query error:" + err);
            } else {
                console.log('createUser', result);
                var ID = result.insertId;
                var query = exports.base.mysql.query(
                    'insert into users (id, name) values (' + ID + ',"' + name + '")',
                    function (err, result) {
                        if (err) {
                            callback(err);
                            console.log("Query error:" + err);
                        } else {
                            console.log('createUser', result, query);
                            callback({login: login, id: ID});
                        }
                    }
                )
            }
        }
    )
}

exports.login = function(login, pass, signUp, name, callBack) {
    subParams = {};

	var query = exports.base.mysql.query(
		"select login, id from auth where login='" + login + "' and pass='" + pass + "'",
		function (err, result) {
			if (err) {
                callBack(err);
                console.log("Query error:" + err);
            }
            console.log(result);
            console.log(result.length);
			if (result.length) {
                if (signUp == true) {
                    callBack({error: ERRORS[2], id: 2});
                } else {
                    subParams = {login: result[0].login};
                    getUserName(result[0].id, callBack);
                }
			} else {
                if (signUp) {
                    existFriend(login,
                        function (result) {
                            if (result.length && result[0].id) {
                                callBack({error: ERRORS[4], id: 4});
                            } else {
                                createUser(login, pass, name, callBack);
                            }
                        }
                    )
                } else {
                    callBack({error: ERRORS[1], id: 1});
                }
			}
		}
	);
};

existFriend = function(phone, callBack) {
	var query = exports.base.mysql.query(
		"select id from auth where login='" + phone + "'",
		function (err, result) {
			if (err) {
                callBack(err);
                console.log("Query error:" + err);
            }
            console.log(result);
			if (result) {
				callBack(result);
			}
		}
	);
};

exports.getObservedFriends = function(userId, callback, subData) {
    var query = exports.base.mysql.query(
        "select observedId from observed where userId=" + userId,
        function (err, result) {
            if (err) {
                callback(err);
                console.log("Query error:" + err);
            }
            console.log(result);
            if (result) {
                var uids = [];
                var length = result.length;

                for (var i = 0; i < length; i++) {
                    uids.push(result[i].observedId);
                }
                if(!uids.length) {
                    callback(uids, subData);
                    return;
                }
                getListUserInfo(uids, callback, subData);
            }
        }
    );
}

existObserved = function(userId, observedId, callBack) {
	var query = exports.base.mysql.query(
        "select id from observed where userId=" + userId + " and observedId=" + observedId,
		function (err, result) {
			if (err) {
                callBack(err);
                console.log("Query error:" + err);
            }
            console.log(result);
			if (result) {
				callBack(result);
			}
		}
	);
};

function getListUserInfo(ids, callback, subData) {
    console.log('select id, name, nick from users where id in(' + ids.join(',') + ')');
    var query = exports.base.mysql.query(
        'select id, name, nick from users where id in(' + ids.join(',') + ')',
        function (err, result) {
            if (err) {
                callback(err);
                console.log("Query error:" + err);
            }
            console.log(result);
            if (result) {
                callback(result, subData);
            }
        }
    );
}

function getUserName(userId, callback) {
    var query = exports.base.mysql.query(
        "select * from users where id=" + userId,
        function (err, result) {
            if (err) {
                callback(err);
                console.log("Query error:" + err);
            }
            console.log(result);
            if (result) {
                mergeSubParams(result[0]);
                callback(result[0]);
            }
        }
    );
}

function createFriend(userId, observedId, callback) {
    var query = exports.base.mysql.query(
        'insert into observed (userId, observedId) values (' + userId + ', ' + observedId + ')',
        function (err, result) {
            if (err) {
                callback(err);
                console.log("Query error:" + err);
            } else {
                callback({id: result.insertId});
            }
        }
    )
}

exports.addFriend = function(id, phone, callBack) {
    subParams = {};
    existFriend(phone,
        function(resultExist) {
            if (resultExist.length && resultExist[0].id) {
                if (resultExist[0].id == id) {
                    callBack({error: ERRORS[3], id: 3});
                    return;
                }
                existObserved(id, resultExist[0].id,
                    function(resultObserved) {
                        if(resultObserved.length && resultObserved[0].id) {
                            callBack({error: ERRORS[2], id: 2})
                        } else {
                            createFriend(id, resultExist[0].id,
                                function(resultCreate) {

                                    getUserName(resultExist[0].id, callBack);
                                }
                            );
                        }
                    }
                )
            } else {
                callBack({error: ERRORS[1], id: 1})
            }
    });
};

function mergeSubParams(child) {
    if (typeof(child) != 'object') return;

    for (var key in subParams) {
        child[key] = subParams[key];
    }
    
    subParams = {};
}

