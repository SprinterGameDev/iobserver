module.exports = BaseCollection;

function BaseCollection() {
    var self = this;
    this._poolModels = new Object();
    this._poolModelsId = new Object();

    this.add = function (socketId, model) {
        self._poolModels[socketId] = model;
        console.log('BaseCollection add ', Object.keys(self._poolModels).length, socketId);
    };

    this.addById = function (userId, socketId) {
            self._poolModelsId[userId] = this._poolModels[socketId];
            console.log('BaseCollection add by id ', Object.keys(self._poolModelsId).length, userId, socketId);
    };

    this.getModel = function(socketId) {
        return this._poolModels[socketId];
    };

    this.getModelById = function(userId) {
        return self._poolModelsId[userId];
    };

    this.modelsById = function () {
            console.log(self._poolModelsId);
            return self._poolModelsId;
    };

    this.models = function () {
        console.log(self._poolModels);
        return self._poolModels;
    };

    this.remove = function(socketId) {
        var model = self._poolModels[socketId];
        if (model) {

            if (model.get('id')) {
                delete self._poolModelsId[model.get('id')];
            }

            delete self._poolModels[socketId];
            model.remove();
            model = null;
        }
    }
}
