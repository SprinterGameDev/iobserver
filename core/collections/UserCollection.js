/**
 * Created by Админ on 13.09.14.
 */
var baseCollection = require('./BaseCollection.js');
var extend = require('../Extend.js');
extend(UserCollection, baseCollection);

module.exports = new UserCollection();

function UserCollection() {
    UserCollection.__super__.constructor.apply(this, arguments);
}
