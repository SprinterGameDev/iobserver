/**
 * Created by Админ on 07.10.14.
 */
var baseCollection = require('./BaseCollection.js');
var extend = require('../Extend.js');
extend(ServiceCollection, baseCollection);

module.exports = new ServiceCollection();

function ServiceCollection() {
    ServiceCollection.__super__.constructor.apply(this, arguments);
}