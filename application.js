﻿var config = require('./core/Config.js');

var app = require(config.MODULE_PATH + 'http').createServer();
var io = require(config.MODULE_PATH +  'socket.io').listen(app);
var crypto = require(config.MODULE_PATH + 'crypto')
var mysql = require(config.MODULE_PATH + 'mysql');
var util = require(config.MODULE_PATH + "util");
var oauth = require('./core/Auth.js');
var base = require('./core/Base.js');
var events = require('./core/Events.js');
var syncController =  require('./core/SyncController.js');
var userModel = require('./core/models/UserModel.js');
var userCollection = require('./core/collections/UserCollection.js');
var serviceModel = require('./core/models/serviceModel.js');
var serviceCollection = require('./core/collections/serviceCollection.js');

var baseInit = new base(mysql);
syncController.applyCollection(serviceCollection, userCollection);
baseInit.init();
oauth.base = baseInit;

// Воспользуемся функциями express для отдачи статики
//app.use(express.static(__dirname + '/public'));
// Обозначим порт для приложения и вебсокета

app.listen(config.APP_LISTEN_PORT);

io.configure('production', function(){
    io.set('browser client minification', true);  // send minified client
    io.set('browser client etag', true);          // apply etag caching logic based on version number

    io.set('transports', [
        'xhr-polling',
        'websocket',
        'htmlfile',
        'flashsocket',
        'jsonp-polling'
    ]);
});

function addAnonymouse(socket) {
    var existUser = userCollection.getModel(socket.id);
    if (existUser) {
        userCollection.remove(socket.id);
    }
    var user = new userModel({socket: socket, socketId: socket.id});
    userCollection.add(socket.id, user);
}

function onSaveSettings(socket) {
    var _self = this;
    this._socket = socket;
    this._existUser = null;
    this.poolRequests = [];
    this.poolResult = {'change': {}};

    this.success = function(json) {
        _self._existUser = userCollection.getModel(_self._socket.id);
        if (json.change) {
            if (json.change.password) {
                var newPassword = json.change.password.new;
                _self.poolRequests.push('change.password');
                oauth.changePassword(_self._existUser.get('id'), newPassword, _self.callback);
            }
            if (json.change.name) {
                _self.poolRequests.push('change.name');
                oauth.changeName(_self._existUser.get('id'), json.change.name.value, _self.callback);
            }
        }
    };

    this.callback = function(data) {
        if (data.password && _self.poolRequests.indexOf('change.password') > -1) {
            _self.poolRequests.splice(_self.poolRequests.indexOf('change.password'), 1);
            _self.poolResult.change.password = data;
        }

        if (data.name && _self.poolRequests.indexOf('change.name') > -1) {
            _self.poolRequests.splice(_self.poolRequests.indexOf('change.name'), 1);
            _self._existUser.set({'name': data.value});
            _self.poolResult.change.name = data;
        }

        if (_self.poolRequests.length <= 0) {
            _self._existUser.sendMessage(events.ON_SAVE_SETTINGS, _self.poolResult);
        }
    };
}

function onConfig(socket) {
    var _self = this;
    this._socket = socket;

    this.success = function(json) {
        var params = {event: events.ON_CONFIG,
            data:
                {
                    appVersion: config.appReleseVersion
                }
        };
        if (_self._socket) {
            _self._socket.emit('event', params);
        }
    };
}

function onLogin(socket) {
    var _self = this;
    this._socket = socket;
    this._existUser = null;

    this.success = function(json) {
        if (json && json.login && json.pass && json.hasOwnProperty('signUp')) {
            var name = json.name ? json.name : "";
            oauth.login(json.login, json.pass, json.signUp, name, _self.loginCallback)
        } else {
            console.error('onLogin', json);
        }
    };

    this.loginCallback = function(data) {
        console.log('loginCallback', data);
        if (typeof(data) == 'object' && data.id && !data.error) {
            _self._existUser = userCollection.getModel(_self._socket.id);
            console.log('user anon->auth');
            if (!_self._existUser) {
                _self._existUser = new userModel({socket: _self._socket});
            }
            _self._existUser.set(data);
            userCollection.addById(data.id, _self._socket.id);
            oauth.getObservedFriends(data.id, _self.onGetObservedFriends);
        } else if (typeof(data) == 'object' && data.error) {                //callback error auth!
            var params = {event: events.ON_AUTH, data: data};
            _self._socket.emit('event', params);
        }
    };

    this.onGetObservedFriends = function(result) {
        var id = _self._existUser.get('id');

        _self._existUser.set({observed: result});
        _self._existUser.generateServiceToken();
        _self._existUser.sendMessage(events.ON_AUTH, _self._existUser.getUserData());
        var params = {event: events.ON_USER_CONNECTED, data: {id: id}};

        io.sockets.emit('event', params);
    };
}

function onAddFriend(socket) {
    var _self = this;
    this._socket = socket;
    var existUser = userCollection.getModel(_self._socket.id);

    if (!existUser || !existUser.isAuthUser()) {
        return;
    }

    this.success = function(json) {
        if (json && json.phone) {
            oauth.addFriend(existUser.get('id'), json.phone , _self.callback)
        } else {
            console.error('onAddFriend', json);
        }
    };

    this.callback = function(data) {
        var params = {};
        if (!data || typeof data == 'string') {
            data = {msg: data};
        }

        console.log('user add', data, (typeof(data) == 'object' && data.id && data.error));
        if(data.id && !data.error) {
            var observed = existUser.get('observed');
            observed.push(data);
            existUser.set({'observed': observed});
            params = data;
        } else {
            params = {error: true, msg: data.error};
        }

        existUser.sendMessage(events.ON_ADD_FRIEND, params);
    };
}

function onLoginService(socket) {
    var _self = this;
    this._socket = socket;

    this.success = function (json) {
        var message = {error: 'token not valid!'};
        console.log("onLoginService", json);
        if (typeof(json) == 'object' && json.id && json.login && json.token ) {
            var existUser = userCollection.getModelById(json.id);
            if (verifyServiceToken(json.token, json.id, json.login)) {
                var model = new serviceModel(
                    {id: json.id, socket: _self._socket, socketId: _self._socket.id}
                );

                if (existUser) {
                    var id = existUser.get('id');

                    model.set({observed: existUser.get('observed')});
                    message = {success: true};
                    userCollection.remove(_self._socket.id);
                    serviceCollection.add(_self._socket.id, model);
                    message = {success: true};
                    var params = {event: events.ON_AUTH_SERVICE, data: message};
                    _self._socket.emit('event', params);
                    params = {event: events.ON_USER_CONNECTED, data: {id: id}};
                    io.sockets.emit('event', params);
                } else {
                    oauth.getObservedFriends(json.id, _self.onGetObservedFriends, model)
                }
            } else {
                var params = {event: events.ON_AUTH_SERVICE, data: message};
                _self._socket.emit('event', params);
            }


        } else {
            var params = {event: events.ON_AUTH_SERVICE, data: message};
            _self._socket.emit('event', params);
        }
    };

    this.onGetObservedFriends = function(result, serviceModel) {
        serviceModel.set({observed: result});
        userCollection.remove(_self._socket.id);
        serviceCollection.add(_self._socket.id, serviceModel);
        var id = serviceModel.get('id');
        var message = {success: true};
        var params = {event: events.ON_AUTH_SERVICE, data: message};
        _self._socket.emit('event', params);
        params = {event: events.ON_USER_CONNECTED, data: {id: id}};
        io.sockets.emit('event', params);
    };
}

function disconnect(socket) {
    var _self = this;
    this._socket = socket;

    this.success = function() {
        var id = 0;

        var existUser = userCollection.getModel(_self._socket.id);
        console.log('disconnect (user):', existUser);
        if (existUser) {
            id = existUser.get('id');
            userCollection.remove(_self._socket.id);
        }

        var existService = serviceCollection.getModel(_self._socket.id);
        console.log('disconnect (service):', existService);
        if (existService) {
            id = existService.get('id');
            serviceCollection.remove(_self._socket.id);
            var params = {event: events.ON_USER_DISCONNECT, data: {id: id}};
            io.sockets.emit('event', params);
        }
    }
}

function onSendCoord(socket) {
    var _self = this;
    this._socket = socket;

    this.success = function(json) {
        console.log("onSendCoord", json);

        var service = serviceCollection.getModel(_self._socket.id);
        if (service && service.get('observed')) {
            syncController.sendMessage(_self._socket.id, json);
        }
    }
}

verifyServiceToken = function(token, id, login) {
        //FIXME!!!!
       var hash = crypto.createHash('md5').update(
               id + login + "DS6DUY4BSU7CXVBGJ36FGVG"
       ).digest("hex");

       return token == hash;
   };


function onTestCallback(result) {
    console.log('onTestCallback', result);
}

io.sockets.on('connection', function (socket) {
    try {
        addAnonymouse(socket);
        socket.on(events.SAVE_SETTINGS, function(json) {new onSaveSettings(socket).success(json)});
        socket.on(events.CONFIG, function(json) {new onConfig(socket).success()});
        socket.on(events.USER_DISCONNECT, function(json) {new disconnect(socket).success()});
        socket.on(events.AUTH_SERVICE, function(json) {new onLoginService(socket).success(json)});
        socket.on(events.AUTH, function(json) {new onLogin(socket).success(json)});
        socket.on(events.ADD_FRIEND, function(json){ new onAddFriend(socket).success(json)});
        socket.on(events.SEND_COORD, function(json){ new onSendCoord(socket).success(json)});
        socket.on('disconnect', function(json){ new disconnect(socket).success()});
    } catch(e){console.log(e)}
});
